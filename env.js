const env = {
    active: 'dev',//preview,dev
    baseUrl: '/design',
    fileUrl: '/file',
    version: '0.9.0',
}
export const baseUrl = env.baseUrl
export const fileUrl = env.fileUrl
export default env
